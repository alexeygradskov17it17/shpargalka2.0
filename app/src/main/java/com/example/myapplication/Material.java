package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class Material extends AppCompatActivity {
    private String text = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);
        WebView webView = findViewById(R.id.webView);
        Intent intent = getIntent();
       String namePlanet = intent.getStringExtra("Planet");
        setTitle(namePlanet);
        text = intent.getStringExtra("nameFile");
        webView.loadUrl("file:///android_asset/"+text);
            }
}
