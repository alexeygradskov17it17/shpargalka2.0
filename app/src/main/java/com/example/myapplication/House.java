package com.example.myapplication;



public class House {
    private int houseResource;
    private String nameOwner;
    private String address;
    private String area;
    private String price;
    private String type;

    House(String nameOwner, String address, String area, String price, String type, int houseResource) {
        this.nameOwner = nameOwner;
        this.address = address;
        this.area =area;
        this.price= price;
        this.type = type;
        this.houseResource = houseResource;
    }

    int getHouseResource() {
        return houseResource;
    }

    String getNameOwner() {
        return nameOwner;
    }

    String getAddress() {
        return address;
    }

    String getArea() {
        return area;
    }

    String getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }
}
