package com.example.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<House> houses = new ArrayList();
    private DataBaseHelper mDBHelper;
    private SQLiteDatabase mDb;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDBHelper = new DataBaseHelper(this);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
        ListView layoutElementOfList = findViewById(R.id.list);
        readDB();
        MyAdapter items = new MyAdapter(this, R.layout.list_view, houses);
        layoutElementOfList.setAdapter(items);
        layoutElementOfList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                final Intent intent = new Intent(MainActivity.this, Material.class);
                startActivity(intent);
            }
        });
    }

    private void readDB() {
        Cursor maxElement = mDb.rawQuery("SELECT MAX('ID') FROM dataOfHouses", null);
        Cursor cursor = mDb.rawQuery("SELECT * FROM dataOfHouses", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            houses.add(new House(cursor.getString(1),cursor.getString(2),
                    cursor.getString(3),cursor.getString(4),
                    cursor.getString(5),cursor.getInt(6)));

            cursor.moveToNext();
        }
cursor.close();
    }


}