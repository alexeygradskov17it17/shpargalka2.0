package com.example.myapplication;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.List;

public class MyAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private int layout;
    private List<House> houses;
    private ImageView photoOfHome;
    private TextView nameOwner;
    private TextView address;
    private TextView area;
    private TextView price;
    private TextView type;
    private House house ;



    public MyAdapter(@NonNull Context context, int resource, @NonNull List<House> houses) {
        super(context, resource, houses);
        this.houses = houses;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);


    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);
        photoOfHome=view.findViewById(R.id.photoOfHome);
        nameOwner = view.findViewById(R.id.nameOwner);
        address = view.findViewById(R.id.adress);
        area = view.findViewById(R.id.area);
        price = view.findViewById(R.id.price);
        type = view.findViewById(R.id.type);
        house = houses.get(position);
        fillData();
        return view;
    }

    private void fillData() {
        photoOfHome.setImageResource(house.getHouseResource());
        nameOwner.setText(house.getNameOwner());
        address.setText(house.getAddress());
        area.setText(house.getArea());
        price.setText(house.getPrice());
        type.setText(house.getType());
    }

}
